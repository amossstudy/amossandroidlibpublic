// Original version Copyright (c) 2013, J. Behar, A. Roebuck, M. Shahid,
// J. Daly, A. Hallack, N. Palmius, K. Niehaus, G. Clifford (University
// of Oxford). All rights reserved.
//
// Modified version Copyright (c) 2014, Nick Palmius (AMoSS Study,
// University of Oxford). All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// NOT MEDICAL SOFTWARE.
// 
// This software is provided for informational or research purposes only,
// and is not for professional medical use, diagnosis, treatment or care,
// nor is it intended to be a substitute therefor. Always seek the advice
// of a physician or other qualified health provider properly licensed to
// practice medicine or general healthcare in your jurisdiction concerning
// any questions you may have regarding any health problem. Never disregard
// professional medical advice or delay in seeking it because of something
// you have observed through the use of this software. Always consult with
// your physician or other qualified health care provider before embarking
// on a new treatment, diet or fitness programme.

// Contact: Nick Palmius (npalmius@googlemail.com)

// Some elements of the AMoSS Study app are confidential and cannot be
// released as open source software.
//
// This is a dummy class to enable compilation of AMoSSAndroidApp without
// access to the AMoSSAndroidLibPrivate repository. Nonin bluetooth pulse
// oximeter communication will not be available.
//
// Original version by J. Behar, A. Roebuck, M. Shahid, J. Daly, A. Hallack,
// N. Palmius, K. Niehaus, G. Clifford

package com.ibme.android.noninmanager;

import java.util.Queue;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.os.Handler;

public class NoninManager extends Thread {
	public NoninManager(Context context, BluetoothAdapter btAdapter, String macAddress, Handler guiHandler) {
	}

	@Override
	public void run() {
	}

	void closeConnections() {
	}

	public void prepareToStop() {
	}

	public boolean isRunning() {
		return false;
	}

	public Queue<Double> getPpgQueue() {
		return null;
	}
}
